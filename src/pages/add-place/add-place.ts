import { SetLocationPage } from './../set-location/set-location';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { Location } from '../../models/location';

@IonicPage()
@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html',
})
export class AddPlacePage {

  location:Location={
    lat:40.7624324,
    lon:-73.9759827
  }
  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public ModalCntrl:ModalController
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPlacePage');
  }

  OnOpenMap(){
    const modal=this.ModalCntrl.create(SetLocationPage,{location:this.location});
    modal.present();
    modal.onDidDismiss((data)=>{
      if(data)
      {
        this.location=data.marker
      }
    })
  }
  OnSubmit(f:NgForm)
  {
    console.log(f.value)
  }
}
