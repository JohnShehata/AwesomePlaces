import { Location } from './../../models/location';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the SetLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html',
})
export class SetLocationPage {
  location:Location;
  marker:Location

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public ViewCntrl:ViewController
    ) {
    console.log(this.navParams.get('location'))
    this.location=this.navParams.get('location')
  }

  OnSetMarker(event:any)
  {
      this.marker=new Location(event.coords.lat,event.coords.lng)
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SetLocationPage');
  }

  OnConfirm()
  {
    this.ViewCntrl.dismiss({marker:this.marker});
  }

  OnAbort()
  {
    this.ViewCntrl.dismiss();
  }

}
